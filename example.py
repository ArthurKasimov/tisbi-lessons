currentHour = 500

workHours = {
    0: False, 1: False, 2: False, 3: False, 4: False, 5: False, 6: False, 7: False, 8: False,
    9: False, 10: True, 11: True, 12: True, 13: True, 14: True, 15: True, 16: True,
    17: True, 18: False, 19: False, 20: False, 21: False, 22: False, 23: False
}

currentHourText = " Текущий час"
isInHour = False

for key, value in workHours.items():
    tmpMsg = ''
    if not value:
        tmpMsg += str(key) + ':00 Это не рабочее время'
    if key == currentHour:
        tmpMsg += currentHourText
        isInHour = True

    if tmpMsg != '':
        print(tmpMsg)

if not isInHour:
    print('Час вне диапазона')
