### Дано
1. Текущее время
```python
currentHour = 4
```
2. Рабочее время
```python
#             [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23]  
workHours = [False, False, False, False, False, False, False, False, False, True, True, True, True, True, True, True, True,
     False, False, False, False, False, False] # range(24)
```

### Задание
1. Сообщить о всех нерабочих часах по отдельности.
2. Если текущее время совпадает, то, дополнительно нужно сообщить, что это текущее время.
3. Если текуще время вне диапазона, сообщить об этом отдельно.

Пример вывода. Он не соотвествует списку workHours. Просто пример.

```text
0:00 - нерабочее время
1:00 - нерабочее время
2:00 - нерабочее время - это текущий час
3:00 - нерабочее время
22:00 - нерабочее время
23:00 - нерабочее время
```